const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();

router.delete('/:filename', (req, res) => {
  try {
    const fileName = req.params.filename;

    fs.readdir(path.join(__dirname, 'files'), (err, files) => {
      if (err) throw err;
      if (files.find(elem => elem === fileName)) {
        fs.unlink(path.join(__dirname, 'files', fileName), err => {
          if (err) throw err;
          res.status(200).json({message: `File ${fileName} was deleted`});
        }) 
      } else {
        res.status(400).json({message: "Bad request"});
      }
    });

  } catch (err) {
    res.status(500).json({message:"Server error"});
  }
});

module.exports = router;