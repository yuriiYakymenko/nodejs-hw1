const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();

router.get('/', (req, res) => {
  try {
    fs.readdir(path.join(__dirname, 'files'), (err, files) => {
      if (err) throw err;
      res.status(200).json({message: "Success", "files": files});
    });
  } catch (err) {
    res.status(500).json({message:"Server error"});
  }
});

module.exports = router;