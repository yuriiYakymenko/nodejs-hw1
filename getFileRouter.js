const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();

router.get('/:filename', (req, res) => {
  try {
    const fileName = req.params.filename;
    
    fs.readFile(path.join(__dirname, 'files', fileName), 'utf-8', (err, data) => {
      if (err) {
        res.status(400).json({message: `No file with '${fileName}' filename found`});
      } else {
        const birthFile = fs.statSync(path.join(__dirname, 'files', fileName)).birthtime;
        const ext = fileName.split('.').pop();

        res.status(200).json({
          message: "Success",
          filename: fileName,
          content: data,
          extension: ext,
          uploadedDate: birthFile
        });
      }
    });
  } catch (err) {
    res.status(500).json({message:"Server error"});
  }
});

module.exports = router;