const express = require('express');
const fs = require('fs');
const path = require('path');
const morgan = require('morgan');
const createFileRouter = require('./createFileRouter');
const getFilesRouter = require('./getFilesRouter');
const getFileRouter = require('./getFileRouter');
const deleteFileRouter = require('./deleteFileRouter');
const app = express();

if (!fs.existsSync(path.join(__dirname, 'files'))) {
  fs.mkdir(path.join(__dirname, 'files'), err => {
    if (err) throw err;
  });
}

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', createFileRouter);
app.use('/api/files', getFilesRouter);
app.use('/api/files', getFileRouter);
app.use('/api/files', deleteFileRouter);

app.use((req, res) => {
  res.status(404).json({message:"Not Found"})
});

app.listen(8080, () => {
  console.log('Server is running');
});
