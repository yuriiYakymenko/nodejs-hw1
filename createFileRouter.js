const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();
const fileExtensions = ['log', 'txt', 'json', 'yaml', 'xml'];

router.post('/', (req, res) => {
  try {
    if (!req.body.hasOwnProperty("content") || req.body.content.length === 0) {
      res.status(400).json({message: "Please specify 'content' parameter"});
    } else if (!req.body.hasOwnProperty("filename")) {
      res.status(400).json({message: "Please specify 'filename' parameter"});
    } else {
      fs.readdir(path.join(__dirname, 'files'), (err, files) => {
        if (err) throw err;
        if (files.find(elem => elem === req.body.filename)) {
          res.status(400).json({message: `File with '${req.body.filename}' filename already exists`})
        } else {
          const re = /(?:\.([^.]+))?$/;
          const extFile = re.exec(req.body.filename)[1];
          const isEqual = fileExtensions.find(elem => elem === extFile);
          if (isEqual) {
            fs.writeFile(
              path.join(__dirname, 'files', req.body.filename),
              req.body.content,
              err => {
                if (err) throw err;
                res.status(200).json({message: "File created successfully"});
            });
          } else {
            res.status(400).json({message:"Please, create file with valid extension"})
          }
        }
      });
    }
  } catch (err) {
    res.status(500).json({message:"Server error"});
  }
});

module.exports = router;